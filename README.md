Translator 3000
===================


Simple free translator with offline and online modes. Online mode uses [Yandex Translate API](https://tech.yandex.ru/translate/doc/dg/reference/translate-docpage/) and for offline mode you can [>download any dictionary from here](http://dicto.org.ru/xdxf.html), parse it using [xdxf ruby parser](https://github.com/hurlenko/xdxf-ruby-parser) or download [already parsed file.](https://github.com/hurlenko/xdxf-ruby-parser/blob/master/out.csv)