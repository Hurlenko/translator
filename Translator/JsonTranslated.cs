﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Translator
{
    [DataContract]
    class JsonTranslated
    {
        [DataMember(Name = "code")]
        public int Code { get; set; }
        [DataMember(Name = "lang")]
        public string Lang { get; set; }
        [DataMember(Name = "text")]
        public List<string> Text { get; set; }
    }
}
