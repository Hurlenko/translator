﻿namespace Translator
{
    partial class MainWindow
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtSource = new System.Windows.Forms.TextBox();
            this.txtTranslated = new System.Windows.Forms.TextBox();
            this.btnTranslate = new System.Windows.Forms.Button();
            this.strpInfo = new System.Windows.Forms.StatusStrip();
            this.lblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.cmbLangFrom = new System.Windows.Forms.ComboBox();
            this.btnSwapLang = new System.Windows.Forms.Button();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cmbLangTo = new System.Windows.Forms.ComboBox();
            this.tabModes = new System.Windows.Forms.TabControl();
            this.tabOnline = new System.Windows.Forms.TabPage();
            this.chkRealTimeTranslation = new System.Windows.Forms.CheckBox();
            this.lblNoInternet = new System.Windows.Forms.Label();
            this.tabOffline = new System.Windows.Forms.TabPage();
            this.btnTranslateOffline = new System.Windows.Forms.Button();
            this.lblChangeLang = new System.Windows.Forms.Label();
            this.btnSwapLangOffline = new System.Windows.Forms.Button();
            this.lblLangB = new System.Windows.Forms.Label();
            this.lblLangA = new System.Windows.Forms.Label();
            this.lblTimeElapsed = new System.Windows.Forms.Label();
            this.lblLoadedWords = new System.Windows.Forms.Label();
            this.txtDelimeter = new System.Windows.Forms.TextBox();
            this.lblDelimeter = new System.Windows.Forms.Label();
            this.lblLoadDictionary = new System.Windows.Forms.Label();
            this.btnLoadDictionary = new System.Windows.Forms.Button();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.ofdLoadDictionary = new System.Windows.Forms.OpenFileDialog();
            this.btnLoadText = new System.Windows.Forms.Button();
            this.btnSaveText = new System.Windows.Forms.Button();
            this.ofdOpentext = new System.Windows.Forms.OpenFileDialog();
            this.sfdSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.strpInfo.SuspendLayout();
            this.tabModes.SuspendLayout();
            this.tabOnline.SuspendLayout();
            this.tabOffline.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSource
            // 
            this.txtSource.AcceptsReturn = true;
            this.txtSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSource.Location = new System.Drawing.Point(11, 97);
            this.txtSource.MaxLength = 2900;
            this.txtSource.Multiline = true;
            this.txtSource.Name = "txtSource";
            this.txtSource.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSource.Size = new System.Drawing.Size(480, 316);
            this.txtSource.TabIndex = 1;
            this.txtSource.TextChanged += new System.EventHandler(this.txtSource_TextChanged);
            this.txtSource.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSource_KeyPress);
            // 
            // txtTranslated
            // 
            this.txtTranslated.AcceptsReturn = true;
            this.txtTranslated.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTranslated.Location = new System.Drawing.Point(493, 97);
            this.txtTranslated.Multiline = true;
            this.txtTranslated.Name = "txtTranslated";
            this.txtTranslated.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTranslated.Size = new System.Drawing.Size(480, 316);
            this.txtTranslated.TabIndex = 1;
            // 
            // btnTranslate
            // 
            this.btnTranslate.Location = new System.Drawing.Point(317, 25);
            this.btnTranslate.Name = "btnTranslate";
            this.btnTranslate.Size = new System.Drawing.Size(75, 23);
            this.btnTranslate.TabIndex = 2;
            this.btnTranslate.Text = "Translate";
            this.btnTranslate.UseVisualStyleBackColor = true;
            this.btnTranslate.Click += new System.EventHandler(this.btnTranslate_Click);
            // 
            // strpInfo
            // 
            this.strpInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblInfo,
            this.ProgressBar});
            this.strpInfo.Location = new System.Drawing.Point(0, 440);
            this.strpInfo.Name = "strpInfo";
            this.strpInfo.Size = new System.Drawing.Size(984, 22);
            this.strpInfo.TabIndex = 3;
            this.strpInfo.Text = "statusStrip1";
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = false;
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.RightToLeftAutoMirrorImage = true;
            this.lblInfo.Size = new System.Drawing.Size(600, 17);
            this.lblInfo.Text = "Type something to translate...";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ProgressBar
            // 
            this.ProgressBar.MarqueeAnimationSpeed = 0;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(200, 16);
            // 
            // cmbLangFrom
            // 
            this.cmbLangFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLangFrom.FormattingEnabled = true;
            this.cmbLangFrom.Items.AddRange(new object[] {
            "Autodetect",
            "Afrikaans-af",
            "Albanian-sq",
            "Arabic-ar",
            "Armenian-hy",
            "Azerbaijani-az",
            "Bashkir-ba",
            "Basque-eu",
            "Belarusian-be",
            "Bosnian-bs",
            "Bulgarian-bg",
            "Catalan-ca",
            "Chinese-zh",
            "Croatian-hr",
            "Czech-cs",
            "Danish-da",
            "Dutch-nl",
            "English-en",
            "Estonian-et",
            "Finnish-fi",
            "French-fr",
            "Galician-gl",
            "Georgian-ka",
            "German-de",
            "Greek-el",
            "Haitian-ht",
            "Hebrew-he",
            "Hindi-hi",
            "Hungarian-hu",
            "Icelandic-is",
            "Indonesian-id",
            "Irish-ga",
            "Italian-it",
            "Japanese-ja",
            "Kazakh-kk",
            "Korean-ko",
            "Kyrgyz-ky",
            "Latin-la",
            "Latvian-lv",
            "Lithuanian-lt",
            "Macedonian-mk",
            "Malagasy-mg",
            "Malay-ms",
            "Maltese-mt",
            "Mongolian-mn",
            "Norwegian-no",
            "Persian-fa",
            "Polish-pl",
            "Portuguese-pt",
            "Romanian-ro",
            "Russian-ru",
            "Serbian-sr",
            "Slovak-sk",
            "Slovenian-sl",
            "Spanish-es",
            "Swahili-sw",
            "Swedish-sv",
            "Tagalog-tl",
            "Tajik-tg",
            "Tatar-tt",
            "Thai-th",
            "Turkish-tr",
            "Udmurt-udm",
            "Ukrainian-uk",
            "Urdu-ur",
            "Uzbek-uz",
            "Vietnamese-vi",
            "Welsh-cy"});
            this.cmbLangFrom.Location = new System.Drawing.Point(6, 26);
            this.cmbLangFrom.Name = "cmbLangFrom";
            this.cmbLangFrom.Size = new System.Drawing.Size(121, 21);
            this.cmbLangFrom.TabIndex = 4;
            // 
            // btnSwapLang
            // 
            this.btnSwapLang.Location = new System.Drawing.Point(133, 25);
            this.btnSwapLang.Name = "btnSwapLang";
            this.btnSwapLang.Size = new System.Drawing.Size(51, 23);
            this.btnSwapLang.TabIndex = 6;
            this.btnSwapLang.Text = "<=>";
            this.btnSwapLang.UseVisualStyleBackColor = true;
            this.btnSwapLang.Click += new System.EventHandler(this.btnSwapLang_Click);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTo.Location = new System.Drawing.Point(187, 7);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(28, 16);
            this.lblTo.TabIndex = 5;
            this.lblTo.Text = "To:";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFrom.Location = new System.Drawing.Point(3, 7);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(42, 16);
            this.lblFrom.TabIndex = 5;
            this.lblFrom.Text = "From:";
            // 
            // cmbLangTo
            // 
            this.cmbLangTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLangTo.FormattingEnabled = true;
            this.cmbLangTo.Items.AddRange(new object[] {
            "Afrikaans-af",
            "Albanian-sq",
            "Arabic-ar",
            "Armenian-hy",
            "Azerbaijani-az",
            "Bashkir-ba",
            "Basque-eu",
            "Belarusian-be",
            "Bosnian-bs",
            "Bulgarian-bg",
            "Catalan-ca",
            "Chinese-zh",
            "Croatian-hr",
            "Czech-cs",
            "Danish-da",
            "Dutch-nl",
            "English-en",
            "Estonian-et",
            "Finnish-fi",
            "French-fr",
            "Galician-gl",
            "Georgian-ka",
            "German-de",
            "Greek-el",
            "Haitian-ht",
            "Hebrew-he",
            "Hindi-hi",
            "Hungarian-hu",
            "Icelandic-is",
            "Indonesian-id",
            "Irish-ga",
            "Italian-it",
            "Japanese-ja",
            "Kazakh-kk",
            "Korean-ko",
            "Kyrgyz-ky",
            "Latin-la",
            "Latvian-lv",
            "Lithuanian-lt",
            "Macedonian-mk",
            "Malagasy-mg",
            "Malay-ms",
            "Maltese-mt",
            "Mongolian-mn",
            "Norwegian-no",
            "Persian-fa",
            "Polish-pl",
            "Portuguese-pt",
            "Romanian-ro",
            "Russian-ru",
            "Serbian-sr",
            "Slovak-sk",
            "Slovenian-sl",
            "Spanish-es",
            "Swahili-sw",
            "Swedish-sv",
            "Tagalog-tl",
            "Tajik-tg",
            "Tatar-tt",
            "Thai-th",
            "Turkish-tr",
            "Udmurt-udm",
            "Ukrainian-uk",
            "Urdu-ur",
            "Uzbek-uz",
            "Vietnamese-vi",
            "Welsh-cy"});
            this.cmbLangTo.Location = new System.Drawing.Point(190, 26);
            this.cmbLangTo.Name = "cmbLangTo";
            this.cmbLangTo.Size = new System.Drawing.Size(121, 21);
            this.cmbLangTo.Sorted = true;
            this.cmbLangTo.TabIndex = 4;
            // 
            // tabModes
            // 
            this.tabModes.Controls.Add(this.tabOnline);
            this.tabModes.Controls.Add(this.tabOffline);
            this.tabModes.Location = new System.Drawing.Point(12, 5);
            this.tabModes.Name = "tabModes";
            this.tabModes.SelectedIndex = 0;
            this.tabModes.Size = new System.Drawing.Size(960, 88);
            this.tabModes.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabModes.TabIndex = 6;
            // 
            // tabOnline
            // 
            this.tabOnline.BackColor = System.Drawing.SystemColors.Control;
            this.tabOnline.Controls.Add(this.chkRealTimeTranslation);
            this.tabOnline.Controls.Add(this.lblNoInternet);
            this.tabOnline.Controls.Add(this.btnSwapLang);
            this.tabOnline.Controls.Add(this.lblTo);
            this.tabOnline.Controls.Add(this.cmbLangTo);
            this.tabOnline.Controls.Add(this.btnTranslate);
            this.tabOnline.Controls.Add(this.cmbLangFrom);
            this.tabOnline.Controls.Add(this.lblFrom);
            this.tabOnline.Location = new System.Drawing.Point(4, 22);
            this.tabOnline.Name = "tabOnline";
            this.tabOnline.Padding = new System.Windows.Forms.Padding(3);
            this.tabOnline.Size = new System.Drawing.Size(952, 62);
            this.tabOnline.TabIndex = 1;
            this.tabOnline.Text = "Online";
            // 
            // chkRealTimeTranslation
            // 
            this.chkRealTimeTranslation.AutoSize = true;
            this.chkRealTimeTranslation.Location = new System.Drawing.Point(317, 5);
            this.chkRealTimeTranslation.Name = "chkRealTimeTranslation";
            this.chkRealTimeTranslation.Size = new System.Drawing.Size(121, 17);
            this.chkRealTimeTranslation.TabIndex = 8;
            this.chkRealTimeTranslation.Text = "Real time translation";
            this.chkRealTimeTranslation.UseVisualStyleBackColor = true;
            this.chkRealTimeTranslation.CheckedChanged += new System.EventHandler(this.chkRealTimeTranslation_CheckStateChanged);
            // 
            // lblNoInternet
            // 
            this.lblNoInternet.Location = new System.Drawing.Point(431, 9);
            this.lblNoInternet.Name = "lblNoInternet";
            this.lblNoInternet.Size = new System.Drawing.Size(286, 50);
            this.lblNoInternet.TabIndex = 7;
            // 
            // tabOffline
            // 
            this.tabOffline.AllowDrop = true;
            this.tabOffline.BackColor = System.Drawing.SystemColors.Control;
            this.tabOffline.Controls.Add(this.btnTranslateOffline);
            this.tabOffline.Controls.Add(this.lblChangeLang);
            this.tabOffline.Controls.Add(this.btnSwapLangOffline);
            this.tabOffline.Controls.Add(this.lblLangB);
            this.tabOffline.Controls.Add(this.lblLangA);
            this.tabOffline.Controls.Add(this.lblTimeElapsed);
            this.tabOffline.Controls.Add(this.lblLoadedWords);
            this.tabOffline.Controls.Add(this.txtDelimeter);
            this.tabOffline.Controls.Add(this.lblDelimeter);
            this.tabOffline.Controls.Add(this.lblLoadDictionary);
            this.tabOffline.Controls.Add(this.btnLoadDictionary);
            this.tabOffline.ForeColor = System.Drawing.SystemColors.Control;
            this.tabOffline.Location = new System.Drawing.Point(4, 22);
            this.tabOffline.Name = "tabOffline";
            this.tabOffline.Padding = new System.Windows.Forms.Padding(3);
            this.tabOffline.Size = new System.Drawing.Size(952, 62);
            this.tabOffline.TabIndex = 2;
            this.tabOffline.Text = "Offline";
            // 
            // btnTranslateOffline
            // 
            this.btnTranslateOffline.Enabled = false;
            this.btnTranslateOffline.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnTranslateOffline.Location = new System.Drawing.Point(564, 29);
            this.btnTranslateOffline.Name = "btnTranslateOffline";
            this.btnTranslateOffline.Size = new System.Drawing.Size(109, 23);
            this.btnTranslateOffline.TabIndex = 7;
            this.btnTranslateOffline.Text = "Translate";
            this.btnTranslateOffline.UseVisualStyleBackColor = true;
            this.btnTranslateOffline.Click += new System.EventHandler(this.btnTranslateOffline_Click);
            // 
            // lblChangeLang
            // 
            this.lblChangeLang.AutoSize = true;
            this.lblChangeLang.ForeColor = System.Drawing.Color.Black;
            this.lblChangeLang.Location = new System.Drawing.Point(419, 12);
            this.lblChangeLang.Name = "lblChangeLang";
            this.lblChangeLang.Size = new System.Drawing.Size(99, 13);
            this.lblChangeLang.TabIndex = 6;
            this.lblChangeLang.Text = "Change languages:";
            // 
            // btnSwapLangOffline
            // 
            this.btnSwapLangOffline.Enabled = false;
            this.btnSwapLangOffline.ForeColor = System.Drawing.Color.Black;
            this.btnSwapLangOffline.Location = new System.Drawing.Point(445, 29);
            this.btnSwapLangOffline.Name = "btnSwapLangOffline";
            this.btnSwapLangOffline.Size = new System.Drawing.Size(42, 23);
            this.btnSwapLangOffline.TabIndex = 5;
            this.btnSwapLangOffline.Text = "<=>";
            this.btnSwapLangOffline.UseVisualStyleBackColor = true;
            this.btnSwapLangOffline.Click += new System.EventHandler(this.btnSwapLangOffline_Click);
            // 
            // lblLangB
            // 
            this.lblLangB.AutoSize = true;
            this.lblLangB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLangB.ForeColor = System.Drawing.Color.Black;
            this.lblLangB.Location = new System.Drawing.Point(493, 31);
            this.lblLangB.Name = "lblLangB";
            this.lblLangB.Size = new System.Drawing.Size(21, 20);
            this.lblLangB.TabIndex = 4;
            this.lblLangB.Text = "B";
            // 
            // lblLangA
            // 
            this.lblLangA.AutoSize = true;
            this.lblLangA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLangA.ForeColor = System.Drawing.Color.Black;
            this.lblLangA.Location = new System.Drawing.Point(418, 31);
            this.lblLangA.Name = "lblLangA";
            this.lblLangA.Size = new System.Drawing.Size(21, 20);
            this.lblLangA.TabIndex = 4;
            this.lblLangA.Text = "A";
            // 
            // lblTimeElapsed
            // 
            this.lblTimeElapsed.AutoSize = true;
            this.lblTimeElapsed.ForeColor = System.Drawing.Color.Black;
            this.lblTimeElapsed.Location = new System.Drawing.Point(227, 31);
            this.lblTimeElapsed.Name = "lblTimeElapsed";
            this.lblTimeElapsed.Size = new System.Drawing.Size(82, 13);
            this.lblTimeElapsed.TabIndex = 3;
            this.lblTimeElapsed.Text = "Time elapsed: 0";
            // 
            // lblLoadedWords
            // 
            this.lblLoadedWords.AutoSize = true;
            this.lblLoadedWords.ForeColor = System.Drawing.Color.Black;
            this.lblLoadedWords.Location = new System.Drawing.Point(227, 12);
            this.lblLoadedWords.Name = "lblLoadedWords";
            this.lblLoadedWords.Size = new System.Drawing.Size(86, 13);
            this.lblLoadedWords.TabIndex = 3;
            this.lblLoadedWords.Text = "Loaded words: 0";
            // 
            // txtDelimeter
            // 
            this.txtDelimeter.Location = new System.Drawing.Point(7, 31);
            this.txtDelimeter.Name = "txtDelimeter";
            this.txtDelimeter.Size = new System.Drawing.Size(67, 20);
            this.txtDelimeter.TabIndex = 2;
            this.txtDelimeter.Text = ";";
            // 
            // lblDelimeter
            // 
            this.lblDelimeter.AutoSize = true;
            this.lblDelimeter.ForeColor = System.Drawing.Color.Black;
            this.lblDelimeter.Location = new System.Drawing.Point(3, 12);
            this.lblDelimeter.Name = "lblDelimeter";
            this.lblDelimeter.Size = new System.Drawing.Size(71, 13);
            this.lblDelimeter.TabIndex = 1;
            this.lblDelimeter.Text = "Set delimeter:";
            // 
            // lblLoadDictionary
            // 
            this.lblLoadDictionary.AutoSize = true;
            this.lblLoadDictionary.ForeColor = System.Drawing.Color.Black;
            this.lblLoadDictionary.Location = new System.Drawing.Point(93, 12);
            this.lblLoadDictionary.Name = "lblLoadDictionary";
            this.lblLoadDictionary.Size = new System.Drawing.Size(117, 13);
            this.lblLoadDictionary.TabIndex = 1;
            this.lblLoadDictionary.Text = "Choose your dictionary:";
            // 
            // btnLoadDictionary
            // 
            this.btnLoadDictionary.ForeColor = System.Drawing.Color.Black;
            this.btnLoadDictionary.Location = new System.Drawing.Point(96, 29);
            this.btnLoadDictionary.Name = "btnLoadDictionary";
            this.btnLoadDictionary.Size = new System.Drawing.Size(114, 23);
            this.btnLoadDictionary.TabIndex = 0;
            this.btnLoadDictionary.Text = "Load Dictionary";
            this.btnLoadDictionary.UseVisualStyleBackColor = true;
            this.btnLoadDictionary.Click += new System.EventHandler(this.btnLoadDictionary_Click);
            // 
            // Timer
            // 
            this.Timer.Interval = 1000;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // ofdLoadDictionary
            // 
            this.ofdLoadDictionary.FileName = "text.txt";
            this.ofdLoadDictionary.Filter = "Text files (*.txt)|*.txt|CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            this.ofdLoadDictionary.FilterIndex = 2;
            this.ofdLoadDictionary.Title = "Select your dictionary";
            // 
            // btnLoadText
            // 
            this.btnLoadText.Location = new System.Drawing.Point(195, 414);
            this.btnLoadText.Name = "btnLoadText";
            this.btnLoadText.Size = new System.Drawing.Size(75, 23);
            this.btnLoadText.TabIndex = 7;
            this.btnLoadText.Text = "Load";
            this.btnLoadText.UseVisualStyleBackColor = true;
            this.btnLoadText.Click += new System.EventHandler(this.btnLoadText_Click);
            // 
            // btnSaveText
            // 
            this.btnSaveText.Location = new System.Drawing.Point(690, 414);
            this.btnSaveText.Name = "btnSaveText";
            this.btnSaveText.Size = new System.Drawing.Size(75, 23);
            this.btnSaveText.TabIndex = 7;
            this.btnSaveText.Text = "Save";
            this.btnSaveText.UseVisualStyleBackColor = true;
            this.btnSaveText.Click += new System.EventHandler(this.btnSaveText_Click);
            // 
            // ofdOpentext
            // 
            this.ofdOpentext.FileName = "text.txt";
            this.ofdOpentext.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            this.ofdOpentext.Title = "Select text file:";
            // 
            // sfdSaveFile
            // 
            this.sfdSaveFile.Filter = "Text files (*.txt)|*.txt";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 462);
            this.Controls.Add(this.btnSaveText);
            this.Controls.Add(this.btnLoadText);
            this.Controls.Add(this.tabModes);
            this.Controls.Add(this.strpInfo);
            this.Controls.Add(this.txtTranslated);
            this.Controls.Add(this.txtSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainWindow";
            this.Text = "Translator";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.strpInfo.ResumeLayout(false);
            this.strpInfo.PerformLayout();
            this.tabModes.ResumeLayout(false);
            this.tabOnline.ResumeLayout(false);
            this.tabOnline.PerformLayout();
            this.tabOffline.ResumeLayout(false);
            this.tabOffline.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSource;
        private System.Windows.Forms.TextBox txtTranslated;
        private System.Windows.Forms.Button btnTranslate;
        private System.Windows.Forms.StatusStrip strpInfo;
        private System.Windows.Forms.ToolStripStatusLabel lblInfo;
        private System.Windows.Forms.ComboBox cmbLangFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.ComboBox cmbLangTo;
        private System.Windows.Forms.ToolStripProgressBar ProgressBar;
        private System.Windows.Forms.Button btnSwapLang;
        private System.Windows.Forms.TabControl tabModes;
        private System.Windows.Forms.TabPage tabOnline;
        private System.Windows.Forms.TabPage tabOffline;
        private System.Windows.Forms.Label lblNoInternet;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.CheckBox chkRealTimeTranslation;
        private System.Windows.Forms.Button btnLoadDictionary;
        private System.Windows.Forms.OpenFileDialog ofdLoadDictionary;
        private System.Windows.Forms.Label lblLoadDictionary;
        private System.Windows.Forms.Label lblLoadedWords;
        private System.Windows.Forms.TextBox txtDelimeter;
        private System.Windows.Forms.Label lblDelimeter;
        private System.Windows.Forms.Label lblTimeElapsed;
        private System.Windows.Forms.Label lblChangeLang;
        private System.Windows.Forms.Button btnSwapLangOffline;
        private System.Windows.Forms.Label lblLangB;
        private System.Windows.Forms.Label lblLangA;
        private System.Windows.Forms.Button btnTranslateOffline;
        private System.Windows.Forms.Button btnLoadText;
        private System.Windows.Forms.Button btnSaveText;
        private System.Windows.Forms.OpenFileDialog ofdOpentext;
        private System.Windows.Forms.SaveFileDialog sfdSaveFile;
    }
}

