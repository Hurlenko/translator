﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Net.NetworkInformation;
using System.Web;
using System.Diagnostics;
using System.Linq;
namespace Translator
{
    public partial class MainWindow : Form
    {
        private readonly string ApiKey    = "trnsl.1.1.20160516T204407Z.9ec57731d0ffa75f.c7722f63ac97197249343885bc841084e2ced105";
        private readonly string reqString = "https://translate.yandex.net/api/v1.5/tr.json/translate?key={0}&text={1}&lang={2}";
        Dictionary<string, string> dictionary = null; // Stores word-translation pairs
        BackgroundWorker bw = new BackgroundWorker();
        Stopwatch sw = new Stopwatch();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void SetInfo(string inf)
        {
            this.lblInfo.Text = inf;
        }

        /*
         * Generating a pair "fromLanCode-toLanCode", "toLanCode" - autodetect the source language
         */
        private string LanguagesPair()
        {
            string langFrom = cmbLangFrom.SelectedItem.ToString();
            string langTo   = cmbLangTo.SelectedItem.ToString();
            string res      = "{0}-{1}";
            if (langFrom == "Autodetect")
            {
                langFrom = string.Empty;
                res = "{0}{1}";
            }
            else
            {
                langFrom = langFrom.Substring(langFrom.Length - 2);
            }
            langTo = langTo.Substring(langTo.Length - 2);
            return string.Format(res, langFrom, langTo);
        }
        /*
         * Online translation method
         */
        private JsonTranslated TranslateOnline()
        {
            string text = txtSource.Text;
            string lang = LanguagesPair();
            JsonTranslated jsonDeserialized = null;
            DataContractJsonSerializer jsonSerialized = new DataContractJsonSerializer(typeof(JsonTranslated));
            if (string.IsNullOrEmpty(text))
            {
                throw new Exception("Nothing to translate!");
            }
            if (string.IsNullOrEmpty(lang))
            {
                throw new Exception("Error while selecting languages");
            }
            text = text.Replace(";", "%3B"); // ; symbol causes some problems while translating
            string requestUrl = string.Format(reqString, ApiKey, text, lang);
            WebRequest request = WebRequest.Create(requestUrl);
            request.Proxy = null;
            WebResponse response = request.GetResponse();
            try
            {
                jsonDeserialized = (JsonTranslated)jsonSerialized.ReadObject(response.GetResponseStream());
            }
            catch (Exception)
            {
                jsonDeserialized = null;
                return null;
            }
            switch (jsonDeserialized.Code)
            {
                case 401: throw new Exception("Wrong API-key");
                case 402: throw new Exception("API-key has been banned");
                case 404: throw new Exception("Daily limit has exceeded");
                case 413: throw new Exception("Max text size exceeded");
                case 422: throw new Exception("Unable to translate");
                case 501: throw new Exception("Translation way is not supported");
                default: return jsonDeserialized;
            }
        }
        /*
         * Setting autodetected language
         */
        private void SetSourceLanguage(string langKey)
        {
            if (langKey.Length == 5)
            {
                langKey = langKey.Substring(0, 2); // Taking first 2 characters from a pair
            }
            foreach (var item in cmbLangFrom.Items)
            {
                string lang = item.ToString();
                if (lang.Substring(lang.Length - 2) == langKey)
                {
                    cmbLangFrom.SelectedItem = item;
                    return;
                }
            }
        }
        /*
         * Updating online translation
         */
        private void UpdateTranslation()
        {
            SetInfo("Translating...");
            JsonTranslated json = this.TranslateOnline();
            if (json != null)
            {
                string translatedText = json.Text[0];
                translatedText = Regex.Replace(translatedText, "(?<!\r)\n", "\r\n");
                this.txtTranslated.Text = translatedText;
                if (cmbLangFrom.SelectedItem.ToString() == "Autodetect")
                {
                    SetSourceLanguage(json.Lang);
                }
            }
            else
            {
                this.txtTranslated.Text = string.Empty;
            }
            SetInfo("Done!");
        }

        private bool IsInternetAvailable()
        {
            try
            {
                WebClient webClient = new WebClient();
                Stream stream = webClient.OpenRead("http://www.google.com");
                stream.Dispose();
                webClient.Dispose();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /*
         * Online translation System events
         */
        private void btnTranslate_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateTranslation();
            }
            catch (Exception ex)
            {
                SetInfo(ex.Message);
            }
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            cmbLangFrom.SelectedIndex = 0;  //Autodetect
            cmbLangTo.SelectedIndex = 62;   //Ukrainian
            if (!IsInternetAvailable())
            {
                tabModes.SelectedIndex = 1; //Offline mode only
                this.tabOnline.Enabled = false; //lock online tab
                this.lblNoInternet.Text = "No Internet connection. Online translation is unavailbale. However you can load your " +
                                          "own dictionaries and keep on using it even being offline. (Hehe)";
            }
        }

        private void btnSwapLang_Click(object sender, EventArgs e)
        {
            int itemp;
            if (cmbLangFrom.SelectedItem.ToString() == "Autodetect")
            {
                try
                {
                    UpdateTranslation();
                }
                catch (Exception ex)
                {
                    SetInfo(ex.Message);
                }
            }
            itemp = cmbLangFrom.SelectedIndex;
            if (itemp == 0) //Autodetect
            {
                return; //Unable to autodetect the source language, cannot swap
            }
            cmbLangFrom.SelectedIndex = cmbLangTo.SelectedIndex + 1; //because of Autodetect
            cmbLangTo.SelectedIndex = itemp - 1;
        }
        /*
         * Calls btnTranslate_Click every 1 second if real-time translation enabled
         */
        private void Timer_Tick(object sender, EventArgs e)
        {
            btnTranslate_Click(sender, e);
            Timer.Enabled = false;
        }

        private void txtSource_TextChanged(object sender, EventArgs e)
        {
            if (chkRealTimeTranslation.CheckState == CheckState.Unchecked)
            {
                return;
            }
            else
            {
                if (!Timer.Enabled)
                {
                    Timer.Enabled = true;
                }
                Timer.Stop();
                Timer.Start();
            }
        }

        private void chkRealTimeTranslation_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkRealTimeTranslation.CheckState == CheckState.Checked)
            {
                btnTranslate_Click(sender, e);
            }
        }

        private void txtSource_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                txtSource.Text = string.Empty;
            }
        }

        /*
         * Offline mode methods 
         */
        private void ParseDictionary(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string dict_path = e.Argument as string;
            Dictionary<string, string> dict = null;
            int count = 0;
            if (!File.Exists(dict_path))
            {
                throw new Exception("File not found");
            }
            string delimeter = txtDelimeter.Text;
            if (string.IsNullOrEmpty(delimeter))
            {
                throw new Exception("Enter the delimeter");
            }
            IEnumerable<string> lines = File.ReadLines(dict_path);

            using (StreamReader r = new StreamReader(dict_path))
            {
                while (r.ReadLine() != null) { count++; }
                dict = new Dictionary<string, string>(count);
            }
            string source_lang = null;
            string translation = null;
            string[] words = null;
            int k = 0;
            foreach (var line in lines)
            {
                words = Regex.Split(line, delimeter);
                source_lang = words[0];
                translation = words[1];
                if (string.IsNullOrEmpty(source_lang) || string.IsNullOrEmpty(translation))
                {
                    continue;
                }
                else
                {

                    dict.Add(source_lang.ToLower(), translation.ToLower());

                }
                int percent = (k + 1) * 100 / count;
                (sender as BackgroundWorker).ReportProgress(percent);
                ++k;
            }
            e.Result = dict;
        }
        private void DictionaryParsed(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null || e.Cancelled)
            {
                SetInfo("Error while parsing, check: delimeter, if file is not locked and has no duplicates");
            }
            else
            {
                dictionary = e.Result as Dictionary<string, string>;
                sw.Stop();
                lblLoadedWords.Text = "Loaded words: " + dictionary.Count.ToString();
                lblTimeElapsed.Text = "Time elapsed: " + sw.Elapsed.ToString("mm\\:ss\\.ff");
                btnLoadDictionary.Enabled = false;
                txtDelimeter.Enabled = false;
                btnTranslateOffline.Enabled = true;
                btnSwapLangOffline.Enabled = true;
                SetInfo("Done");
            }
            ProgressBar.Value = 0;
            sw.Stop();
        }
        void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressBar.Value = e.ProgressPercentage;
        }
        /*
         * Offline translation method
         */
        private void TranslateOffline()
        {
            string source = txtSource.Text;
            string processed = string.Empty;
            if (string.IsNullOrEmpty(source))
            {
                throw new Exception("Nothing to translate");
            }
            source = Regex.Replace(source, @"\r\n?|\n", "\n");
            source = Regex.Replace(source, "\n", "\r\n ");
            string[] sourceSplitted = source.Split(' ');
            string pattern = "[\\~#%&*{}/:<>?|\"-.,\\n\\r]";
            Regex regEx = new Regex(pattern);
            string found = string.Empty;
            string result = string.Empty;
            foreach (string word in sourceSplitted)
            {
                string wordNew = Regex.Replace(regEx.Replace(word, string.Empty), @"\s+", string.Empty);
                if(string.IsNullOrEmpty(word))
                {
                    continue;
                }
                string translated = string.Empty;
                if (dictionary.TryGetValue(wordNew.ToLower(), out found))
                {
                    if (word.Contains(wordNew))
                    {
                        translated = word.Replace(wordNew, found);
                        if (wordNew.All(char.IsUpper))
                        {
                            translated = translated.ToUpper();
                        }
                        else if (char.IsUpper(wordNew[0]))
                        {
                            translated = translated.First().ToString().ToUpper() + translated.Substring(1);
                        }
                    }
                    else
                    {
                        translated = word;
                    }
                }
                else
                {
                    translated = word;
                }
                result += translated + " ";
            }
            txtTranslated.Text = result;
        }
        /*
         * Offline mode sSystem events
         */
        private void LoadText(string path)
        {
            const Int32 BufferSize = 1024;
            string result = string.Empty;
            using (var fileStream = File.OpenRead(path))
            {
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
                {
                    String line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        result += line + System.Environment.NewLine;
                    }
                }
            }
            txtSource.Text = result;
        }
        private void btnLoadDictionary_Click(object sender, EventArgs e)
        {
            if (bw.IsBusy)
            {
                return;
            }
            else if (ofdLoadDictionary.ShowDialog() == DialogResult.OK)
            {

                try
                {
                    bw.DoWork += new DoWorkEventHandler(ParseDictionary);
                    bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(DictionaryParsed);
                    bw.ProgressChanged += new ProgressChangedEventHandler(ProgressChanged);
                    bw.WorkerReportsProgress = true;
                    SetInfo("Loading");
                    sw.Start();
                    bw.RunWorkerAsync(ofdLoadDictionary.FileName);
                }
                catch (Exception ex)
                {
                    SetInfo(ex.Message);
                }
            }
        }

        private void btnTranslateOffline_Click(object sender, EventArgs e)
        {
            try
            {
                TranslateOffline();
            }
            catch (Exception ex)
            {
                SetInfo(ex.Message);
            }
        }

        private void btnSwapLangOffline_Click(object sender, EventArgs e)
        {
            int oldSize = dictionary.Count;
            string temp = string.Empty;
            try
            {
                dictionary = dictionary.ToLookup(kp => kp.Value).ToDictionary(g => g.Key, g => g.First().Key);
            }
            catch (Exception ex)
            {
                SetInfo(ex.Message);
            }
            SetInfo((oldSize - dictionary.Count).ToString() + " pairs lost because of duplicates");
            lblLoadedWords.Text = "Loaded words: " + dictionary.Count.ToString();
            temp = lblLangA.Text;
            lblLangA.Text = lblLangB.Text;
            lblLangB.Text = temp;
        }

        private void btnLoadText_Click(object sender, EventArgs e)
        {
            if (ofdOpentext.ShowDialog() == DialogResult.OK)
            {
                SetInfo("Loading...");
                try
                {
                    LoadText(ofdOpentext.FileName);
                }
                catch (Exception ex)
                {
                    SetInfo(ex.Message);
                }
                SetInfo("Done");
            }
        }

        private void btnSaveText_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTranslated.Text))
            {
                SetInfo("Nothing to save");
                return;
            }
            else if (sfdSaveFile.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string[] lines = txtTranslated.Lines;
                    SetInfo("Saving..");
                    using (StreamWriter sw = new StreamWriter(sfdSaveFile.FileName))
                    {
                        foreach (string line in lines)
                        {
                            sw.WriteLine(line);
                        }
                    }
                    SetInfo("Saved");
                }
                catch (Exception ex)
                {
                    SetInfo(ex.Message);
                }
            }
        }
    }
}